set -e

test -f /mnt/MiSTer_config && cp -v /mnt/MiSTer_config Linux-Kernel_MiSTer/.config
make -C Linux-Kernel_MiSTer menuconfig
make -C Linux-Kernel_MiSTer --quiet zImage modules socfpga_cyclone5_de10_nano.dtb
cat > zImage_dtb \
  Linux-Kernel_MiSTer/arch/arm/boot/zImage \
  Linux-Kernel_MiSTer/arch/arm/boot/dts/socfpga_cyclone5_de10_nano.dtb
make -C Linux-Kernel_MiSTer --quiet modules_install
tar -cf /mnt/modules.tar -C / lib/modules
cp -v zImage_dtb /mnt/zImage_dtb
cp -v Linux-Kernel_MiSTer/.config /mnt/MiSTer_config

if test -f "/mnt/minimigirq.tar.gz"
  then
    echo "/mnt/minimigirq.tar.gz found!"
    tar xf "/mnt/minimigirq.tar.gz"
    if (cd "minimigirq/" && make && cd "/root")
      then
        echo "Building /mnt/minimig-irq.ko succeded!"
        cp -v "minimigirq/minimig-irq.ko" "/mnt/minimig-irq.ko"
      else
        echo "Building /mnt/minimig-irq.ko failed!"
        bash -l
    fi
fi
