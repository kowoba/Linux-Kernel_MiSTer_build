FROM debian:bullseye
WORKDIR /root/
#SHELL ["/bin/bash", "-c"]
ENV ARCH=arm
ENV EXTRAVERSION=-MiSTer
ENV CROSS_COMPILE=/root/gcc-arm-10.2-2020.11-x86_64-arm-none-linux-gnueabihf/bin/arm-none-linux-gnueabihf-
RUN (apt-get update; apt-get -y upgrade; apt-get -y install build-essential git libncurses-dev flex bison openssl libssl-dev dkms libelf-dev libudev-dev libpci-dev libiberty-dev autoconf liblz4-tool bc curl gcc git libssl-dev libncurses5-dev lzop make u-boot-tools libgmp3-dev libmpc-dev wget) >/dev/null 2>&1
RUN wget -q -O - https://developer.arm.com/-/media/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-arm-none-linux-gnueabihf.tar.xz | tar xJf -
RUN git clone --depth=1 https://github.com/MiSTer-devel/Linux-Kernel_MiSTer.git
RUN make -C Linux-Kernel_MiSTer --quiet clean mrproper MiSTer_defconfig zImage modules socfpga_cyclone5_de10_nano.dtb
RUN echo "docker run -v \$(pwd):/mnt -ti mister_kernel"
CMD ["/bin/bash", "/mnt/build.sh"]
#CMD ["/bin/bash"]
